function ExpandableSection(toggleButton) {
  var section = document.getElementById(toggleButton.getAttribute("data-expands"));
  var sectionHeight = section.scrollHeight + "px";
  var COLLAPSED_HEIGHT = 0;

  function collapseSection() {
    updateSectionHeight();

    requestAnimationFrame(function() {
      section.style.height = COLLAPSED_HEIGHT;
    });

    // show 'SHOW' button text
    toggleButton.classList.remove("expanded");
    // collapse section
    section.classList.remove("expanded");
  }

  function expandSection() {
    updateSectionHeight()

    section.style.height = sectionHeight;

    // show 'HIDE' button text
    toggleButton.classList.add("expanded");
    // expand section
    section.classList.add("expanded");
  }

  function updateSectionHeight() {
    sectionHeight = section.scrollHeight + "px";

    section.style.height = sectionHeight;
  }

  // set height manually to avoid animation on page load
  section.style.height = COLLAPSED_HEIGHT;

  addEventListener('resize', function() {
    if (section.classList.contains('expanded')) {
      section.style.height = null;

      updateSectionHeight();
    }
  });

  // delegate toggle function to button
  toggleButton.addEventListener('click', function(e) {
    e.preventDefault();

    section.classList.contains('expanded') ? collapseSection() : expandSection();
  });
}

document.addEventListener("DOMContentLoaded", function() {
  [].slice.call(document.querySelectorAll("[data-expands]")).forEach(function(toggleButton) {
    return new ExpandableSection(toggleButton);
  });
});

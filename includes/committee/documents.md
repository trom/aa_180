## {{ aac-documents[heading] Download documents from the committee }}

|    |    |
| -- | -- |
| <time datetime="2019-08-02">{{ aac-documents-1[date] 2019, AUGUST 02 }}</time> {.date} | {{ aa-documents-1[document title] <a href="files/acceptable-ads-committee-bylaws-2019.pdf" target="_blank" download="Committee Bylaws 2019">Committee Bylaws 2019 (Version 3.0)</a> }} |
| <time datetime="2018-08-13">{{ aac-documents-2[date] 2018, AUGUST 13 }}</time> {.date} | {{ aa-documents-2[document title] <a href="files/acceptable-ads-committee-bylaws-2018.pdf" target="_blank" download="Committee Bylaws 2018">Committee Bylaws 2018 (Version 2.0)</a> }} |
| <time datetime="2018-03-26">{{ aac-documents-3[date] 2018, MARCH 26 }}</time> {.date} | {{ aa-documents-3[document title] <a href="files/acceptable-ads-committee-mobileads-study.pdf" target="_blank" download="Mobile Advertising Study">Mobile Advertising Study - Measuring ad-blocking users' perceptions of advertising types on mobile browsers)</a> }} |
| <time datetime="2017-03-15">{{ aac-documents-4[date] 2017, MARCH 15 }}</time> {.date} | {{ aa-documents-4[document title] <a href="files/acceptable-ads-committee-bylaws.pdf" target="_blank" download="Committee Bylaws 2017">Committee Bylaws 2017 (Version 1.0)</a> }} |
| <time datetime="2017-03-15">{{ aac-documents-5[date] 2017, MARCH 15 }}</time> {.date} | {{ aa-documents-5[document title] <a href="files/acceptable-ads-committee-structure.jpg" target="_blank" download="Committee Structure 2017">Committee Structure 2017</a> }} |

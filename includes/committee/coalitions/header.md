## {{ coalitions-header[header] The AAC is divided into three coalitions: For-profit, User Advocate, and Expert }}

{{ coalitions-1 Each coalition consists of several Groups. Groups are based upon the specific segment they represent, like the advertisers group or the digital rights organizations group. Each Group includes a Representative and multiple supporting Members. }}
 

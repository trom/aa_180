## {{ standards-size-heading[Standards placement heading] <span>03</span> Size }}

{{ standards-size There are strict sizes for individual ads, as well as the total space occupied by ads. All ads that are visible in the browser window when the page first loads must not collectively occupy more than 15% of the visible portion of the web page. If placed lower on the page, ads must not collectively occupy more than 25% of the visible portion of the webpage. }}

{{ standards-size-list Ads must also comply with size limitations, according to the ad position: }}

  - {{ standards-size-200px-high 200px high when above the Primary Content }}
  - {{ standards-size-350px-wide 350px wide when on the side of the Primary Content }}
  - {{ standards-size-400px-high 400px high when placed below the Primary Content }}

{{ standards-size-2 Ads must always leave sufficient space for the Primary Content on the common screen size of 1366x768 for desktop, 360x640 for mobile devices and 768x1024 for tablets. }} [^1] [^2] [^3]

{{ standards-size-3 All ads that are placed above the fold (the portion of the webpage visible in the browser window when the page first loads under the common screen size) must not occupy in total more than 15 percent of the visible portion of the web page. If placed below the fold, ads must not occupy in total more than 25 percent of the visible portion of the webpage. }}

[^1]: {{ standard-size-footnote-1 The 'common screen size' for desktop is 1366x768, based on data from <a href="https://gs.statcounter.com/#desktop-resolution-ww-monthly-201401-201412" class="no-scroll">StatCounter</a>. }}
[^2]: {{ standard-size-footnote-2 The 'common screen size' for mobile is 360x640, based on data from <a href="https://gs.statcounter.com/#mobile_resolution-ww-monthly-201401-201412" class="no-scroll">StatCounter</a>. }}
[^3]: {{ standard-size-footnote-3 The 'common screen size' for tablets is 768x1024, based on data from <a href="https://gs.statcounter.com/#tablet-resolution-ww-monthly-201401-201412" class="no-scroll">StatCounter</a>. }}

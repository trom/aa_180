## {{ standards-heading[heading] Placement, distinction, and size }}

{{ standards-criteria-heading[heading] Acceptable Ads must comply with the following criteria to be shown to ad-blocking users. }}
{: .subheading }

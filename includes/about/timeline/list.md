<time datetime="2006">2006</time>
:   {{ 2006[event] Adblock Plus is written and released. It becomes the most popular Firefox extension in less than a year. }}

<time datetime="2011">2011</time>
:   {{ 2011[event] eyeo, the company behind Adblock Plus, is founded. The Acceptable Ads initiative is born. }}

<time datetime="2012">2012</time>
:   {{ 2012[event] The original Acceptable Ads criteria is developed by eyeo and the Adblock Plus community. By working together, both parties create a standard for better ads that benefits both users and advertisers. }}

<time datetime="2017">2017</time>
:   {{ 2017[event] The Acceptable Ads Committee is formed. eyeo hands over the Acceptable Ads criteria to the Committee. }}

<time datetime="2018">2018</time>
:   {{ 2018[event] The Acceptable Ads Committee releases Mobile Acceptable Ads criteria based on a study by an independent research company. }}

<time datetime="2019">2019</time> <span class="and-beyond">& beyond</span>
:   {{ 2019[event] The Acceptable Ads Committee turns its attention to video ads and more. }}

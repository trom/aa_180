#### {{footer-col-3-header[Footer column 3 header] Important info}}

- [{{footer-privacy-policy[Text of the link in the footer] Privacy policy}}](privacy)

- [{{footer-imprint[Text of the link in the footer] Imprint}}](imprint)

- [{{footer-imprint[Text of the link in the footer] Documents}}](committee#documents){: .no-scroll }
